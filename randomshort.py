import socketserverd
import sys

import webapp
import string
import random
import shelve
from urllib import parse


PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Introduzca una url a acortar:</p>
    <form action="/" method="POST">
      <input name="url" type="text" />
    <input type="submit" value="Submit" />
    </form>
    <p>Lista de urls:</p>
    <p>{resource}</p>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {recurso}.</p>
    <p>Introduzca una url a acortar:</p>
    <form action="/" method="POST">
      <input name="" type=url"text" />
    <input type="submit" value="Submit" />
    </form>
    <p>Lista de urls:</p>
    <p>{resource}</p>
  </body>
</html>

  </body>
</html>
"""

class randomshort(webapp.webApp):
    lista = shelve.open('lista_urls',writeback=True)
    urls={}
    urls['lista']=lista
    recurso =''
    if len(urls['lista'])!=0:
        for key in urls['lista']:
            recurso = recurso + '<br>' + str(key) + ': ' + str(urls['lista'][str(key)]) + '</br>'


    def parse(self, request):
        #print(request)
        respuesta = request.split(' ', 3)[0], request.split(' ', 3)[1], request
        return respuesta

    def process(self, resource):
        #for key in self.urls:
         #   self.recurso = self.recurso+ str(key)+': '+str(self.urls[key])+'\r\n'

        if resource[0] == 'GET':
            if resource[1] == '/':
                page = PAGE.format(resource=self.recurso)
                code = "200 OK \r\n"

            elif len(self.urls)!=0:
                contenido = False
                for key in self.urls['lista']:
                    if resource[1] == key:
                        contenido = True

                if contenido:
                    print(str(self.urls['lista'][str(resource[1])]))
                    page = 'Location: ' + str(self.urls['lista'][str(resource[1])])
                    code = "301 Moved permanently"

                else:
                    page = PAGE_NOT_FOUND.format(resource=self.recurso,recurso=resource[1])
                    code = "404 HTTP ERROR"
            else:
                page = PAGE_NOT_FOUND.format(resource=self.recurso,recurso=resource[1])
                code = "404 HTTP ERROR"

            return (code, page)

        elif resource[0] == 'POST':
            if resource[1] == '/':
                print(str(resource[2]))
                data = {}
                body_start = resource[2].find('\r\n\r\n')
                if body_start == -1:
                    data['body'] = None
                else:
                    data['body'] = resource[2][body_start + 4:]
                fields = parse.parse_qs(data['body'])
                if ('url' in fields):
                    content = parse.unquote(fields['url'][0])
                    if str(content[0:7]) != 'http://' and str(content[0:8]) != 'https://':
                        content = 'https://'+str(content)

                    confirmed = False
                    if len(self.urls)!=0:
                        for key in self.urls['lista']:
                            if content == self.urls['lista'][str(key)]:
                                confirmed = True

                    if confirmed:
                        page = PAGE.format(resource=self.recurso)
                        code = "200 OK \r\n"

                    else:
                        acortada = '/'+(''.join(random.choices(string.ascii_lowercase + string.digits, k=7)))
                        self.urls['lista'][str(acortada)] = str(content)
                        self.recurso = self.recurso + '<br>'+str(acortada)+': '+str(self.urls['lista'][str(acortada)])+'</br>'
                        self.lista = self.urls['lista']
                        self.lista.sync()
                        page = PAGE.format(resource=self.recurso)
                        code = "200 OK \r\n"
                else:
                    page = PAGE.format(resource=self.recurso)
                    code = "200 OK \r\n"
            else:
                page = PAGE_NOT_FOUND.format(resource=self.recurso,recurso=resource[1])
                code = "404 HTTP ERROR"
            return (code, page)

if __name__ == "__main__":
    testrandomshort = randomshort("localhost", 1234)

